from microbit import *
import neopixel
import utime


class Wheels:
    def absign(self, num): return (num, 0) if num >= 0 else (1023 + num, 1)

    def wrpins(self, pns, states):
        pns[1].write_digital(states[1])
        pns[0].write_analog( states[0])

    def __init__(self, lspd=pin0, ldir=pin8, rspd=pin1, rdir=pin12):
        self.lpns, self.lcur = (lspd, ldir), (0,0)
        self.rpns, self.rcur = (rspd, rdir), (0,0)

    def drive(self, lspd, rspd):
        self.lcur, self.rcur = self.absign(lspd), self.absign(rspd)
        self.wrpins(self.lpns, self.lcur)
        self.wrpins(self.rpns, self.rcur)


class Lights:
    def __init__(self, pin=pin13):
        self.L, m = neopixel.NeoPixel(pin, 12), 255
        self.C = dict(zip('rgbytpw',[(m,0,0),(0,m,0),(0,0,m),(m,m,0),(0,m,m),(m,0,m),(m,m,m)]))

    def col(self, v):
        return (lambda t:self.C[v]if t==str else((v,v,v)if t==int else v))(type(v))

    def on(self, lefts, rights):
        lefts = lefts + [-1] * (6-len(lefts))
        rights = rights + [-1] * (6-len(rights))
        for i,v in enumerate(lefts+rights): self.L[i] = (self.L[i] if v in (-1, None) else self.col(v))
        self.L.show()

    def decay(self, by=0.33, subt=5):
        for i in range(12): self.L[i] = tuple(max(int(v*(1-by))-subt, 0) for v in self.L[i])
        self.L.show()

    def state(self):
        A = [self.L[i] for i in range(12)]
        return A[:6], A[6:]

    def clear(self):
        self.L.clear()


class LightSensors:
    def read(self):
        sw, rd = pin16.write_digital, pin2.read_analog
        _, l = sw(0), rd()
        _, r = sw(1), rd()
        return l, r


class Execr:
    def __init__(self):
        self.reg = {}

    def exec_every(self, jobtitle, ifelapsedms, func, *args, **kwargs):
        now = utime.ticks_ms()
        last = self.reg.get(jobtitle, None)
        if last is None or (now > last + ifelapsedms):
            self.reg[jobtitle] = now
            return func(*args, **kwargs)
        else:
            return None

wheels = Wheels()
lights = Lights()
sensors = LightSensors()
execr = Execr()
lights.clear()


if __name__ == "__main__":

    def nacelle(colour=(15, 0, 20), trace=0.45):
        L, _ = lights.state()
        _, h_idx = max((v, n) for n,v in enumerate(L))
        L = [tuple(int(v*trace) for v in l) for l in L]
        L[(h_idx - 1) % 6] = colour
        lights.on(L, L)

    def approach_light(threshold, base, diff):
        light_l, light_r = sensors.read()
        if   light_l > light_r + threshold:
            wheels.drive(base-diff, base+diff)
        elif light_l + threshold < light_r:
            wheels.drive(base+diff, base-diff)
        else:
            wheels.drive(base, base)

    while True:
        execr.exec_every('nacelle',        200, nacelle, colour=(1, 0, 2))
        execr.exec_every('approach light',   5, approach_light, 2, 300, 300)
